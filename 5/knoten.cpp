#include<iostream>
#include<cstring>

struct Graph{

	static int lastValue;
	static int c;
	int value; std::string name; Graph*  node[12]; //Vektor der Länge 12 wird erzeugt und liest Pointer auf Objekte der Klasse Graph ein
	Graph (int value, std::string name) : value(value), name(name){};//Konstruktor
	void getmax(){		
		Graph *ptr;
		int low{200};
		for (int i=0;i<12;i++)//Vergleich wird 12 mal durchgeführt
		{//vergleiche kleinsten aktuellen Wert und den kleinsten  Wert aus der letzten Rukursion mit den Werten aller anderen Objekte und speichere den nächstkleinsten Wert in ptr
			if(low>(*node[i]).value && (*node[i]).value>lastValue){
				ptr=&(*node[i]);
				low=(*node[i]).value;
				};
		}
		std::cout << (*ptr).name << " ";//Ausgabe der Bezeichnung des nächst-kleinsten Werts
		lastValue=(*ptr).value;//Setze "lastvalue" auf den kleinsten Wert aller Objekte, damit dieses nicht mehr betrachtet wird 
		c++;//c speichert, dass eine weitere Rekursion durchgeführt wurde
		if (c<12){//Abbruchkriterium
		(*ptr).getmax();//Rekursion mit aktuellem Zeiger auf den nächstkleineren Wert
		}	
	}

};

int Graph::lastValue=-200;//setze lastValue auf einen Wert, der kleiner als alle anderen Werte ist
int Graph::c=0;//Setze Variable in der Anzahl der Rekursionen gespeichert ist auf 0

void getmax2(Graph* x,Graph* y,int lv,int d,int l){
        for (int i=0;i<12;i++)//Vergleich wird 12 mal durchgeführt
        {//vergleiche kleinsten aktuellen Wert und den kleinsten  Wert aus der letzten Rukursion mit den Werten aller anderen Objekte und speichere den nächstkleinsten Wert in ptr
               if(l>((*(*y).node[i])).value && (*((*y).node[i])).value>lv){
                  	x=&(*((*x).node[i]));
                        l=(*x).value;
                        };
                }
                std::cout << (*x).name << " ";//Ausgabe der Bezeichnung des nächst-kleinsten Werts
                lv=(*x).value;//Setze "lv" auf den kleinsten Wert aller Objekte, damit dieses nicht mehr betrachtet wird 
                d++;//d speichert, dass eine weitere Rekursion durchgeführt wurde
                if (d<12){//Abbruchkriterium
                getmax2(x,x,lv,d,l=200);//Rekursion mit aktuellem Zeiger auf den nächstkleineren Wert
                }
}
int main (){
	//Objekterzeugung mit den vorgegebenen Werten
	Graph node_1(42,",");
	Graph node_2(137,"\n");
	Graph node_3(-5,"dieser");
	Graph node_4(2,"Satz");
	Graph node_5(22,"ergibt");
	Graph node_6(17,"Sinn");
	Graph node_7(61,"ist");
	Graph node_8(67,"Lösung");
	Graph node_9(-49,"Wenn");
	Graph node_10(65,"die");
	Graph node_11(99,".");
	Graph node_12(81,"korrekt");
	Graph *g[12]{&node_1,&node_2,&node_3,&node_4,&node_5,&node_6,&node_7,&node_8,&node_9,&node_10,&node_11,&node_12};//Weist Vektor die Adressen der Objekte zu

	for (int i=0;i<12;i++)//Jeder Knoten erhält Pointer auf alle Knoten
	{
		node_1.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_2.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_3.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_4.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_5.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_6.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_7.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_8.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_9.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_10.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_11.node[i]=g[i];
	}
	for (int i=0;i<12;i++)
	{
		node_12.node[i]=g[i];
	}
	node_1.getmax();
	getmax2(&node_1, &node_1,-200,0,200);
	return 0;
}
