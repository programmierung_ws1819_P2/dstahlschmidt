#include<iostream>
#include<cstdlib>

using namespace std;

struct list_element {
	const double value;
	list_element *prev, *next;

  // TODOs
  list_element(double value): value(double(rand())), prev(nullptr), next(nullptr){}
 	
  void print_fwd(){
	cout << value << endl;}	
  void print_bwd(){
  	cout << value << endl;}
};

struct list {
	list_element *head, *tail, *run;
	size_t length;
  // TODOs
  list(size_t a): length(a){
  	tail= new list_element(double());
	head=tail;
	list_element* current=head;
	for (int i=1;i<length;i++){
		tail->next=new list_element(double());
		tail=tail->next;
		tail->prev=current;
		current=current->next;
	}

  }
  ~list(){
	for (int i=0;i<length;i++){  
		list_element* current=head;
		head=head->next;
		delete current;
	}
  }
  void swap(list_element* p){
        if (p->prev != nullptr)
        {
            if (p == head->next)
            {
                head = p;
            }
            if (p == tail)
            {
                tail = p->prev;
            }
            
            const list_element buffer(*p);
            
            if (buffer.prev->prev != nullptr)
            {
                buffer.prev->prev->next = p;
            }
            
            if (p->next != nullptr)
            {
                buffer.next->prev = buffer.prev;
            }
            
            buffer.prev->next = buffer.next;
            p->prev = buffer.prev->prev;
            buffer.prev->prev = p;
            p->next = buffer.prev;
        }
        else
        {
            cout << "Kein Tausch möglich. Objekt ist erstes Element." << endl;
        }
	}


	  
	  
  void sort(){
	  int vts=0;
	  for(int i=1;i<length;i++){
		  list_element* current=head;
		  for(int j=0;j<i;j++){
			  current=current->next;}
		  while(current->value < current->prev->value){
			  swap(current);
			  vts++;
			  if (current->prev==nullptr){break;}
		  }	
	     
  	  }		  
	  cout << "Durchgeführte Vertauschungen: " << vts << endl;
		

	  }

  void print_fwd() {
    list_element* current=head;
    for(size_t i=0;i<length;i++){
    	current->print_fwd();
    	current=current->next;}
    delete current;
  }

  void print_bwd() {
    list_element* current=tail;
    for(size_t i=0;i<length;i++){
    	current->print_bwd();
    	current=current->prev;}
    delete current;	
  }
};
int main() {
	cout << "Eingabe der Listengroesse: \n";
	size_t len; cin >> len;
	cout << endl;
  list *l = new list(len);
	l->print_fwd();
	l->sort();
  l->print_bwd();
	delete l;
	return 0;
}
