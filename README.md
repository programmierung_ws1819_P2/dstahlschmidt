AUFGABE 1
11111111 11111111 11111111 11111001 // Binärdarstellung der ganzen Zahl -7 (Zweierkomplement von 0...0111, zunächst wird das Einerkomplement gebildet, also 0en und 1en komplementiert und dann 1 addiert)) 
00000000 00000000 00000000 01111111 // Binärdarstellung der ganzen Zahl 127 
01111111 11111111 11111111 11111111 // Binärdarstellung der größtmöglichen ganzen Zahl des Variabentyps integer (2147483647)
10000000 00000000 00000000 00000000 // Da eine größere Zahl als die größtmögliche ausgegeben werden soll, findet ein Overflow statt und es wird durch Addition der Binärdarstellungen von INT_MAX und 1 die Binärdarstellungvon -2147483638 (Zweierkomplement) ausgegeben.
4444 //Ausgabe der Größe (jeweils 4 Byte) der Werte -7,127, INT_MAX und INT_MAX+1 des Variablentyps integer (ganze Zahlen)
11000000 11100000 00000000 00000000
01000010 11111110 00000000 00000000 
01001111 00000000 00000000 00000000
//Binäre Ausgabe der Gleitkommazahlen einfacher Genauigkeit -7, 127 und der größtmöglichen Zahl (3.40282e+38).Die erste Ziffer bestimmt das Vorzeichen (1 entspricht -,0 entspricht +), die folgenden 8 Bit legen den Expronenten fest (bei 127 besipielsweise 10000101,also 2^(133-127)) und die hinteren 23 Bit die Mantisse (bei 127 entspricht diese 63/64; die Gleitkommazahl lässt sich folglich darstellen als +(63/64)*2^7)   
11001111 00000000 00000000 00000000 //Ausgabe von FLT_MAX+1, es entsteht durch Addition einer Zahl größer 0 mit FLT_MAX ein Overflow, der dazu führt dass die Ausgabe nicht richtig interpretiert werden kann. Die Binärausgabe würde als -2.14748e+09 interpretiert werden.
4444 //Ausgabe der Größe (jeweils 4 Byte) der Werte -7,127, FLT_MAX und FLT_MAX+1 des Variablentyps float (Gleitkommazahlen einfacher Genauigkeit)
11000000 00011100 00000000 00000000 00000000 00000000 00000000 00000000 
01000000 01011111 11000000 00000000 00000000 00000000 00000000 00000000 
01000001 11011111 11111111 11111111 11111111 11000000 00000000 00000000
//Analog zu float wird double ausgegeben, allerdinigs mit einem Bit für das Vorzeichen, 11 Bit für den Exponenten und 52 Bit für die Mantisse, wodurch die Zahlen auf ungefähr 16 Kommastellen genau ausgegeben werden können. 
11000001 11100000 00000000 00000000 00000000 00000000 00000000 00000000 //Analag zu float gibt es einen Overflow mit Ausgabe der selben Zahl, was sich auf die Art der Addition von Gleitkommazahlen zurückführen lässt und dass es keine größere darstellbare Zahl als DBL_MAX gibt.
8888 //Ausgabe der Größe (jeweils 8 Byte) der Werte -7,127, DBL_MAX und DBL_MAX+1 des Variablentyps double (Gleitkommazahlen doppelter Genauigkeit)


AUFGABE 2
// Werte für float, der Überlauf findet zwischen den Stellen 354.8 und 354.9 statt
(354|1)
(354.1|1)
(354.2|1)
(354.3|1)
(354.4|1)
(354.5|1)
(354.6|1)
(354.7|1)
(354.8|1)
(354.9|-nan)
(355|-nan)
(355.1|-nan)
(355.2|-nan)
(355.3|-nan)
(355.4|-nan)
(355.5|-nan)
(355.6|-nan)
(355.7|-nan)
(355.8|-nan)
(355.9|-nan)
//Wert für double mit d=DBL_MAX, es findet kein Überlauf statt.
(1.79769e+308|1)
//Werte für long double, der Überlauf findet zwischen den Werten 354.8 (mit y=1) und 354.9 (mit nicht definiertem y)statt
(354|1)
(354.1|1)
(354.2|1)
(354.3|1)
(354.4|1)
(354.5|1)
(354.6|1)
(354.7|1)
(354.8|1)
(354.9|-nan)
(355|-nan)
(355.1|-nan)
(355.2|-nan)
(355.3|-nan)
(355.4|-nan)
(355.5|-nan)
(355.6|-nan)
(355.7|-nan)
(355.8|-nan)
(355.9|-nan)

Diese Werte stimmen aber nicht mit der Berechnung überein, weil es den ersten Overflow gibt wenn FLT_MAX, DBL_MAX und LDBL_MAX gleich exp(2*x) ist. Für float läge der Overflow Wert bei ca. 44.36 (ln(FLT_MAX-1)/2),für Double bei ln(DBL_MAX-1)/2 und für long double bei ln(LDBL_MAX-1)/2 . Merkwürdigerweise wird für Long-Double früher ein -nan ausgegeben, als bei Double, obwohl Long Double höhere Genauigkeit hat.

AUFGABE 3
Der Wert für die exakte beträgt 7.36718. Die Werte für double und long double entsprechen diesem bei den Approxiationen, bei float weichen sie je nach Differenzenquotient um bis zu 1.18673 ab.
