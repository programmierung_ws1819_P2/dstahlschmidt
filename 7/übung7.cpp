#include <iostream>
#include<fstream>
#include<cassert>

using namespace std;

// TODO: Implementierung des Gaussschen Eliminationsverfahrens
template<typename T>
struct gauss{
	static int it;
	int n;
	int m;
	int l;
	T** t;
	T* v;
	T* vl = nullptr;
	gauss(string a, string b){
		it++;
		if(it>3) throw runtime_error("Maximal 3 Instanzen können erzeugt werden!");
		ifstream in1(a);
		in1 >> n;
		in1 >> m;
		if(m!=n) {throw runtime_error("A ist nicht quadratisch!");}
		t = new T* [n];
		for (int i=0;i<n;i++){
			t[i]=new T [m];
			for (int j=0;j<m;j++){
				in1 >> t[i][j];
			}
		in1.close();
		}
		ifstream in2(b);
		in2 >> l;
		if(m!=l) {throw runtime_error("A und b haben unterschiedliche Dimensionen");}
		v=new T [l];
		for (int e=0;e<l;e++){
			in2 >> v[e];
		}
		in2.close();
	}
	~gauss(){
		it--;
		for(int i=0;i<n;i++){
			delete t[i];
		}
		delete t;
		delete v;
		delete vl;
	}
	int solve(){
		vl = new T [n];
		for (int i=n-1;i>=0;i--){
			T* x = new T;
			*x=v[i];
			for (int j=i+1;j<=n;j++){
				*x=(*x)-(t[i][j])*(vl[j]);
			}
			if(t[i][i]==0) throw runtime_error("Diagonalelement ist gleich 0!");
			*x=*x/t[i][i];
			vl[i]=*x;
			delete x;
		}	
	}
	void write(string w){
		if (vl==nullptr) throw runtime_error("Lösungsvektor wurde noch nicht berechnet!");
		ofstream out(w);
		for(int i=0;i<m;i++){
			out << vl[i] << endl;
		}
	}	
};

template<typename T> int gauss<T>::it=0;

void test1() {
  gauss<double> A("A.txt", "b.txt");
  gauss<double> B("A2.txt", "b2.txt");
  gauss<double> C("A.txt", "b.txt");
  gauss<double> D("A.txt", "b.txt");
}

void test2() {
  gauss<double> A("A.txt", "b.txt");
  A.solve();
  A.write("x.txt");
}

void test3() {
  gauss<double> A("A2.txt", "b2.txt");
  A.write("x.txt");
}

void test4() {
  gauss<double> A("A2.txt", "b2.txt");
  A.solve();
}

void test5() {
  gauss<double> A("A3.txt", "b2.txt");
  A.solve();
}

void test6() {
  gauss<double> A("A2.txt", "b.txt");
  A.solve();
}

int main() {
  cout << "test 1" << endl;
  try {
    test1();
  } catch (exception& E) {
    cout << "Exception: " << E.what() << endl;
  }
  cout << "test 2" << endl;
  try {
    test2();
  } catch (exception& E) {
    cout << "Exception: " << E.what() << endl;
  }
  cout << "test 3" << endl;
  try {
    test3();
  } catch (exception& E) {
    cout << "Exception: " << E.what() << endl;
  }
  cout << "test 4" << endl;
  try {
    test4();
  } catch (exception& E) {
    cout << "Exception: " << E.what() << endl;
  }
  cout << "test 5" << endl;
  try {
    test5();
  } catch (exception& E) {
    cout << "Exception: " << E.what() << endl;
  }
  cout << "test 6" << endl;
  try {
    test6();
  } catch (exception& E) {
    cout << "Exception: " << E.what() << endl;
  }

	return 0;
}
