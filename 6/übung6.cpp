#include<iostream>

using namespace std;

template<typename T, typename S>
struct element {
	T x; S y;
	void init(const T& t,const S& s){
		x=t; y=s;
	}
	void set_first(const T& t){
		x=t;
	}
	T& get_first(){
		T& a = x;
		return a;
	}
	void set_second(const S& s){
		y=s;
	}
	S& get_second(){
		S& b = y;
		return b;
	}
	void print(){
		cout << "(" << x << "," << y << ")" << endl;
	}	
};


struct queue {
	element<int, double> q[10];
        int to;

        queue(): to(-1){}
        void push(const element<int, double>& e){
                if (to==9){
                        q[to]=e;
                }
                else
                        to++;
                        q[to]=e;
        }
        void pop(){
                to--;
        }
        element<int, double>& top(){
                element<int, double>& a = q[to];
                return a;
        }
        int size(){
                return (to+1);
        }
        void print(){
                for (int i=0;i<=to;i++){
                        q[i].print();
                        }
        }

};

int main() {
  int n_elements = 30;

  queue Q;

  for(int i=0; i<n_elements/2; i++) {
    for (int j=0; j<2; j++) {
      element<int,double> e;
      e.init(2*i+j,2*i+j);
      Q.push(e);
    }
    Q.print();
    Q.pop();
    Q.print();
  }
	auto& E = Q.top();
	E.set_first(-1);
	E.print();
	cout << endl;
	Q.print();
  return 0;
}


