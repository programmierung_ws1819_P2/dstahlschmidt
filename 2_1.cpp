#include <cstring>
#include <iostream>
#include <climits>
#include <cfloat>
#include <cmath>

using std::cout;
using std::endl;


template<class T>
void to_bin(T v)
{
	union
	{
		T value;
		unsigned char bytes[sizeof(T)];
	};
	memset(&bytes, 0, sizeof(T));
	value=v;
	// assumes little endian machine 
	for (size_t i = sizeof(T);i>0;i--)
	{
		unsigned char pot=128;
		for (int j = 7;j>=0;j--,pot/=2) 
			if (bytes[i-1]&pot) 
				cout << "1";
			else 
				cout << "0";
		cout << " ";
	}
	cout << endl;
}

int main() {
	int a=-7;int b=127;int c=INT_MAX;int d=INT_MAX+1;
	to_bin(-7); to_bin(127); to_bin(INT_MAX);to_bin(INT_MAX+1);
	std::cout << sizeof(a) << sizeof(b) << sizeof(c) << sizeof(d) << std::endl;
	auto af=static_cast<float>(a); auto bf=static_cast<float>(b); auto cf=static_cast<float>(c); auto df=static_cast<float>(d);
	to_bin(af); to_bin(bf); to_bin(cf);to_bin(df);  
	std::cout << sizeof(af) << sizeof(bf) << sizeof(cf) << sizeof(df)  << std::endl;
	auto da=static_cast<double>(a);auto db=static_cast<double>(b);auto dc=static_cast<double>(c);auto dd=static_cast<double>(d);
	to_bin(da); to_bin(db); to_bin(dc); to_bin(dd);
       	std::cout << sizeof(da) << sizeof(db) << sizeof(dc) << sizeof(dd) << std::endl;
	std::cout << df << dd << std::endl;	
	return 0;
}
