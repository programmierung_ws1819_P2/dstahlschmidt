#include<iostream>
#include<cmath>
#include<cfloat>

float ffunc(float x)
{
	return x+exp(cos(5*x));
}

double dfunc(long double x)
{
	return x+exp(cos(5*x));
}

long double ldfunc(long double x) 
{
	return x+exp(cos(5*x));
}
float fquotienten(float x){
	float h =0.1; //sqrt(FLT_EPSILON);
	std::cout << "FLOAT Vorwärtsdifferenzenquotient " << (ffunc(x+h)-ffunc(x))/h << std::endl;
	std::cout << "FLOAT Rückwärtsdifferenzenquotient " << (ffunc(x)-ffunc(x-h))/h << std::endl;
	std::cout << "FLOUT Zentraler Differenzenquotient " << (ffunc(x+h)-ffunc(x-h))/(2*h) << std::endl;
	return 0;
}
double dquotienten(double x)
{
	double h = sqrt(DBL_EPSILON);
        std::cout << "DOUBLE Vorwärtsdifferenzenquotient " << (dfunc(x+h)-dfunc(x))/h << std::endl;
        std::cout << "DOUBLE Rückwärtsdifferenzenquotient " << (dfunc(x)-dfunc(x-h))/h << std::endl;
        std::cout << "DOUBLE Zentraler Differenzenquotient " << (dfunc(x+h)-dfunc(x-h))/(2*h) << std::endl;
	return 0;
}
long double ldquotienten(long double x)
{
	float h = sqrt(LDBL_EPSILON);
        std::cout << "LONG DOUBLE Vorwärtsdifferenzenquotient " << (ldfunc(x+h)-ldfunc(x))/h << std::endl;
        std::cout << "LONG DOUBLE Rückwärtsdifferenzenquotient " << (ldfunc(x)-ldfunc(x-h))/h << std::endl;
        std::cout << "LONG DOUBLE Zentraler Differenzenquotient " << (ldfunc(x+h)-ldfunc(x-h))/(2*h) << std::endl;
	return 0;
}
long double differential(long double x)
{ 
	std::cout << "Genaue Ableitung " << 1-5*sin(5*x)*exp(cos(5*x)) << std::endl;
}
int main(){
	fquotienten(1);
	dquotienten(1);
	ldquotienten(1);
	differential(1);
	return 0;
}

