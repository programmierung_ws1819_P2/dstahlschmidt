#include<iostream>
#include<fstream>
#include<cmath>

typedef unsigned int uint;

const unsigned int a=500;
const unsigned int b=500;

void imultiplication(double matrix[500][500],double vector[500], double * &max)//Implementierung mittels Indizes
{
	double g = 0;
	std::ofstream p("b1.txt");
	for (int j=0;j<b;j++)
	{
		g=0;
		for (int i=0; i<a; i++)
		{	
			g=g+((matrix[j][i])*(vector[i]));
			if (matrix[j][i] >= *max)
			{
				max = &matrix[j][i];
			}
		}
		std::cout << g << std::endl;
		p << g << std::endl;
	}
	p.close();
}
void pmultiplication(double matrix[500][500],double vector [500], double * &max)//Implementierung mittels Zeiger
{
	double g = 0;
	std::ofstream o("b2.txt");
        for (int j=0;j<b;j++)
        {
                g=0;
                for (int i=0; i<a; i++)
                {
                        g=g+(*(*(matrix+j)+i))*(*(vector+i));
			if (*(*(matrix+j)+i) >= *max)
			{
				max = *(matrix+j)+i;
			}
                }
                std::cout << g << std::endl;
		o << g << std::endl;
        }
	o.close();
}

		 	
int main()
{
	double matrix[a][b];
	std::ifstream in("A.txt");
	for (int c=0; c<a;c++)
	{
		for (int d=0; d<b;d++)
		{
			in >> matrix[c][d];
		}
	}
	in.close();
	double vector[b];
	in.open("x.txt");
	for (int h=0;h<b;h++)
	{
		in >> vector[h];
	}
	in.close();
	double *max=&matrix[0][0];
	imultiplication(matrix,vector, max);
	std::cout << "Maximaler Wert der Matrix: " << *max << std::endl; std::cout << "Adresse des maximalen Werts: " << max << std::endl;
	max=&matrix[0][0];
	pmultiplication(matrix,vector, max);
	std::cout << "Maximaler Wert der Matrix: " << *max << std::endl; std::cout << "Adresse des maximalen Werts: " << max << std::endl;
	return 0;
}
	
