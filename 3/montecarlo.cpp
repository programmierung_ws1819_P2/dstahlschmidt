#include<iostream>
#include<cmath>
#include<random>
#include<time.h>
#define _USE_MATH_DEFINES
double f1(double x){
	return cos((M_PI/2)*x)+3;
}
double f2(double x){
        return 2*x+6;
}
double f3(double x){
        return 3*x*x+6;
}
double f4(double x){
        return 18;
}
double f(double x){
        if (-4<=x<=-2){return f1(x);}
	if (-2<x<=0){return f2(x);}
	if (0<x<=2){return f3(x);}
	if (2<x<=4){return f4(x);}
	else {return 0;}//Funktion f
}

double zufallszahl(int a, int b){
	static std::default_random_engine generator(time(0));
	std::uniform_real_distribution<double> distribution(a,b);
	double random_number = distribution(generator);
	return random_number;//Zufallszahlengenerator
}
int main(){
	for (int n=10;n<10000;n=n*10){
		int m=0;
		for(int i=0;i<n;i++){
		if (zufallszahl(0,20)<=f(zufallszahl(-4,4))){m=m+1;}	//Jedes Mal wenn ein Zufallswert unter dem Graphen auftritt, wird m um 1 inkrementiert.
		}
		double a = static_cast<double>(n);
		double b = static_cast<double>(m);
		std::cout << "Schätzung des Integrals für " << n << " Zufallswerte: "  << (b/a)*(8*20) << std::endl;//Ausgabe des genäherten Integrals
		std::cout << "Relativer Fehler für " << n  << " Zufallswerte: " << (((b/a)*(8*20)-70)/70) << std::endl;//Ausgabe des Verhältnisses zwischen Näherung und genauem Integral
	}
	return 0;
}
