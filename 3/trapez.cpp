#include<iostream>
#include<cmath>
#define _USE_MATH_DEFINES
double f1(double x){
        return cos((M_PI/2)*x)+3;
}
double f2(double x){
        return 2*x+6;
}
double f3(double x){
        return 3*x*x+6;
}
double f4(double x){
        return 18;
}
double f(double x){
        if (-4<=x<=-2){return f1(x);}
        if (-2<x<=0){return f2(x);}
        if (0<x<=2){return f3(x);}
        if (2<x<=4){return f4(x);}
        else {return 0;}
}

int main(){
        for (int n=10;n<=1000;n=n*10){
                double d=0;
		double s=0;
		double r = static_cast<double>(n);
                for(double i=-4;i<4;i=i+(8/r)){
                	if (f(i)<=f(i+(8/r))){//Fallunterscheidung, um je nach dem ob f(i+8/r) kleiner oder größer als i ist, Dreiecksfläche zu addieren oder subtrahieren
			d = (f(i)*(8/r))+(((f(i+(8/r))-f(i))*(8/r))/2);//Recktecksläche -  Dreiecksfläche
			s = s+d;
			}
			if (f(i)>f(i+(8/r))){
                        d = (f(i)*(8/r))-(((f(i)-f(i+(8/r)))*(8/r))/2);//Rechtecksfläche + Dreiecksfläche 
                        s = s+d;
                        }

		}
                std::cout << "Näherung des Integrals: "<< s << std::endl;
                std::cout << "Relativer Fehler: " << (s-70)/70  << std::endl;
        }
        return 0;
}
                                                 
