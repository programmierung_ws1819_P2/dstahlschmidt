#include<iostream>
#include<cmath>
#include<cfloat>

auto tanh(auto x) {return (exp(2*x)-1)/(exp(2*x)+1);}

int main(){
	float f=354;
	double d=DBL_MAX;
	long double ld=354;
	for (f; f<356;f=(f+0.1)){
		std::cout << "(" <<  f << "|" << tanh(f) << ")" << std::endl;
	}
	std::cout << "(" <<  d << "|" << tanh(d) << ")" << std::endl;
	for (ld; ld<356;ld=(ld+0.1)){
                std::cout << "(" <<  ld << "|" << tanh(ld) << ")" << std::endl;

	}
	return 0;
}
