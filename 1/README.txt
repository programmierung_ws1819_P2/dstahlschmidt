Kompilerschritte:
g++ -E hello.cpp  führt den Präprozessor aus und gibt Programm mit zusätzlichen Kommentaren in der Konsole aus,
es wird noch keine Datei erstellt.
g++ -c hello.cpp erstellt Objektdatei (.o) und führt Kompilierung durch. Der Linker wird noch nicht aufgerufen.
g++ hello.o vollständige Kompilierung der in g++ -c erstellten Datei und ausführbare Datei a.out wird erstellt.**
g++ -o hello hello.cpp wie **, allerdings heißt die ausführbare Datei jetzt hello.
g++ -Wall -Werror hello.cpp führt eine vollständige Kompilierung durch und gibt zusätzlich Informationen über auftretende Fehler und Warnungen.

Schritte bis zur ausführbaren Datei:
-Erstellung einer .cpp Datei und Schreiben eines kompilierbaren Codes (vim *****.cpp)
-Speichern des Codes in der erstellten .cpp Datei (>:wq)
-Kompilierung eines Codes mittels g++ und gewünschten zusätzlichen Optionen (siehe hello.cpp)
-Ausführbare Datei a.out wird generiert
-Ausführung der Datei mit ./a.out

Kompilierung und Ausführung von Klassik.cpp

-Kompilierung mittels g++ -Wall -Werror -o klassik.exe klassik.cpp um Fehler und Warnungen anzuzeigen
und eine von anderen ausführbaren Dateien unterscheidbare Datei zu generieren
-Erstellung einer points.dat Datei, mit n x-y Werten und einer Variable für n am Anfang der Datei
-Ausführung mittels ./klassik.exe

Führt man das Programm mit den Punkten aus dem L2P (2,3),(1,2),(1.5,1) aus, so liefert es die Werte
p=0.7365 und den neuen Punkt (1.5,1.6572).
