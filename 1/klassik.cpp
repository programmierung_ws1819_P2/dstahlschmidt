#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>

typedef unsigned int uint;

float model(float p, float x)
{
	return p * x * x;
}

int main()
{
	float p = 0;
	float x = 0;
	float y = 0;

	std::ifstream in("points.dat");
	uint pointcount;
	in >> pointcount;
	
	float sum1 = 0;
	float sum2 = 0;
	
	std::vector<float> olddatX(pointcount, 0);
	std::vector<float> olddatY(pointcount, 0);

	for(uint i = 0; i < pointcount; i++)
	{
		in >> x >> y;
		olddatX[i] = x;
		olddatY[i] = y;
		sum1 += y * x * x;
		sum2 += pow(x, 4);
	}

	p = sum1 / sum2;

	std::cout << "Neues x: ";
	std::cin >> x;
	
	std::cout << "p = " << p << std::endl;
	std::cout << "y = p * x * x = " << model(p, x) << std::endl;
	std::cout << "Wahres y: ";
	std::cin >> y;
	
	in.close();

	std::ofstream out("points.dat");
	out << ++pointcount<< std::endl;
	for (uint i = 0; i < pointcount - 1; i++)
	{
		out << olddatX[i] << " " << olddatY[i] << std::endl;
	}
	out << x << " " << y << std::endl;

	out.close();
	
	return 0;
}
